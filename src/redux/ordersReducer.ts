import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { IOrder } from "src/interfaces";

export interface IOrdersState {
	orders: IOrder[];
	loadingOrders: boolean;
}

const initialState: IOrdersState = {
	orders: [],
	loadingOrders: false,
};

const ordersSlice = createSlice({
	name: "orders",
	initialState,
	reducers: {
		setOrdersLoading: (state, action: PayloadAction<boolean>) => {
			state.loadingOrders = action.payload;
		},
		setOrders: (state, action: PayloadAction<{ orders: IOrder[] }>) => {
			const { orders } = action.payload;
			state.orders = orders;
			state.loadingOrders = false;
		},
		updateOrder: (state, action: PayloadAction<{ order: IOrder }>) => {
			const { order } = action.payload;
			const index: number = state.orders.findIndex((o: IOrder) => o._id === order._id);
			state.orders[index] = order;
		},
	},
});

export const { setOrdersLoading, setOrders, updateOrder } = ordersSlice.actions;
export default ordersSlice;
