import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { IPizza } from "src/interfaces";

export interface ICartState {
	cart: IPizza[];
}

const initialState: ICartState = {
	cart: [],
};

const cartSlice = createSlice({
	name: "cart",
	initialState,
	reducers: {
		addToCart: (state, action: { payload: { pizza: IPizza } }) => {
			const cart: IPizza[] = [...state.cart, action.payload.pizza];
			state.cart = cart;
		},
		removeFromCart: (state, action: PayloadAction<{ pizza: IPizza }>) => {
			const { _id } = action.payload.pizza;
			const cart = state.cart.filter((c) => c._id !== _id);
			state.cart = cart;
		},
		clearCart: (state) => {
			state.cart = [];
		},
	},
});

export const { addToCart, removeFromCart, clearCart } = cartSlice.actions;
export default cartSlice;
