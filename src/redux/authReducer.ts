import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { IUser } from "src/interfaces";

export interface IAuthState {
	user: IUser;
	token: string;
}

const initialState: IAuthState = {
	user: null,
	token: "",
};

const authSlice = createSlice({
	name: "auth",
	initialState,
	reducers: {
		setToken: (state, action: PayloadAction<{ token: string }>) => {
			const { token } = action.payload;
			state.token = token;
		},
		setUser: (state, action: PayloadAction<{ user: IUser }>) => {
			const { user } = action.payload;
			state.user = user;
		},
		clearUser: (state) => {
			state.user = null;
			state.token = null;
		},
	},
});

export const { setUser, clearUser, setToken } = authSlice.actions;
export default authSlice;
