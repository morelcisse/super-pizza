import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { IUser } from "src/interfaces";

export interface IUserState {
	users: IUser[];
	loadingUsers: boolean;
}

const initialState: IUserState = {
	users: [],
	loadingUsers: false,
};

const usersSlice = createSlice({
	name: "users",
	initialState,
	reducers: {
		setUsersLoading: (state, action: PayloadAction<boolean>) => {
			state.loadingUsers = action.payload;
		},
		createUser: (state, action: PayloadAction<{ user: IUser }>) => {
			const { user } = action.payload;
			state.users = [...state.users, user];
			state.loadingUsers = false;
		},
		updateUser: (state, action: PayloadAction<{ user: IUser }>) => {
			const { user } = action.payload;
			const index = state.users.findIndex((p: IUser) => p._id === user._id);
			state.users[index] = action.payload.user;
			state.loadingUsers = false;
		},
		deleteUser: (state, action: PayloadAction<{ user: IUser }>) => {
			const { user } = action.payload;
			const users = state.users.filter((u) => u._id !== user._id);
			state.users = users;
			state.loadingUsers = false;
		},
		setUsers: (state, action: PayloadAction<{ users: IUser[] }>) => {
			const { users } = action.payload;
			state.users = users;
			state.loadingUsers = false;
		},
	},
});

export const { setUsersLoading, createUser, updateUser, deleteUser, setUsers } = usersSlice.actions;
export default usersSlice;
