import { useEffect } from "react";
import { Routes, Route } from "react-router-dom";

import Loading from "src/components/Loading/Loading";
import Dashboard from "src/components/Dashboard/Dashboard";
import Home from "src/components/Home/Home";
import Navbar from "src/components/Navbar/Navbar";

import { updateAppLoading } from "src/redux/appReducer";
import { useDispatch, useSelector } from "src/redux";

function App() {
	const loading = useSelector(({ app }) => app.loading);
	const dispatch = useDispatch();

	useEffect(() => {
		setTimeout(() => dispatch(updateAppLoading(false)), 1500);
	}, []);

	if (loading) return <Loading />;

	return (
		<>
			<Navbar />

			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/dashboard" element={<Dashboard />} />
			</Routes>
		</>
	);
}

export default App;
