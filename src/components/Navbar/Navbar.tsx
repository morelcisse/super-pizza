import { useState } from "react";
import { NavLink } from "react-router-dom";
import Swal from "sweetalert2";

import { IPizza } from "src/interfaces";
import AddAdmin from "src/components/Navbar/components/AddAdmin";
import { useDispatch, useSelector } from "src/redux";
import { clearUser, setToken, setUser } from "src/redux/authReducer";
import { clearCart, removeFromCart } from "src/redux/cartReducer";
import Modal from "../Modal/Modal";
import axiosInstance from "../../helpers/Axios";

import "./Navbar.scss";

function Navbar() {
	const user = useSelector(({ auth }) => auth.user);
	const cart = useSelector(({ cart }) => cart.cart);
	const [loginModal, setLoginModal] = useState(false);
	const [registrationModal, setRegistrationModal] = useState(false);
	const [isLogin, setIsLogin] = useState(false);
	const dispatch = useDispatch();
	const [values, setValues] = useState({ username: "", email: "", password: "" });
	const total = cart.reduce((n, { price }) => n + price, "");
	const handleChange = (evt: any) => {
		const { name, value } = evt.target;

		setValues({ ...values, [name]: value });
	};
	// eslint-disable-next-line consistent-return
	const handleSubmit = async () => {
		const path = isLogin ? "/login" : "/registration";
		const response = await axiosInstance().post(`/auth${path}`, {
			user: { ...values, role: "Utilisateur" },
		});
		const { data } = response;
		const { token, message } = data;

		if (!isLogin) {
			if (message === "User already exist") {
				return Swal.fire({
					title: "Erreur",
					text: "Cet utilisateur existe déjà.",
					icon: "error",
					confirmButtonText: "OK",
				});
			}
			setValues({ username: "", email: "", password: "" });
			setRegistrationModal(false);
			return Swal.fire({
				title: "Succès",
				text: "Votre inscription a été effectuée avec succès. Vous pouvez vous connecter.",
				icon: "success",
				confirmButtonText: "OK",
			});
		}

		if (message !== "User logged in") {
			return Swal.fire({
				title: "Erreur",
				text: "Vos identifiants ne sont pas corrects.",
				icon: "error",
				confirmButtonText: "OK",
			});
		}

		dispatch(setToken(token));
		dispatch(setUser(data.user));
		setLoginModal(false);
		return Swal.fire({
			title: "Connexion",
			text: "Vous êtes authentifié(e) !",
			icon: "success",
			confirmButtonText: "OK",
		});
	};

	const handleOrder = async () => {
		const pizzasIds = cart.map((c: IPizza) => c._id);
		const response = await axiosInstance().post("/orders/create", {
			pizzasIds,
			userId: user._id,
		});
		const { data } = response;
		const { message } = data;

		if (message === "Order waiting") {
			return Swal.fire({
				title: "En attente",
				text: "Votre commande à été mise en attente. Vous serez notifiez par mail dès sa prise en charge.",
				icon: "warning",
				confirmButtonText: "OK",
			});
		}
		if (message === "Order in progress") {
			return Swal.fire({
				title: "En cours de traitement",
				text: "Votre commande à été prise en compte. Vous serez notifiez par mail dès qu'elle est prête.",
				icon: "success",
				confirmButtonText: "OK",
			});
		}

		return dispatch(clearCart());
	};

	const logout = async () => {
		await axiosInstance()
			.post("/auth/logout")
			.then((response) => {
				const { data } = response;

				if (data.message === "User deconnected") {
					dispatch(clearUser());
					Swal.fire({
						title: "Déconnexion",
						text: "Vous êtes déconnecté(e) !",
						icon: "success",
						confirmButtonText: "OK",
					}).then((res) => {
						if (res.isConfirmed) window.location.href = "/";
					});
				}
			});
	};

	return (
		<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
			<div className="container-fluid">
				<button
					className="navbar-toggler"
					type="button"
					data-bs-toggle="collapse"
					data-bs-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent"
					aria-expanded="false"
					aria-label="Toggle navigation">
					<span className="navbar-toggler-icon" />
				</button>
				<div className="collapse navbar-collapse" id="navbarSupportedContent">
					<ul className="navbar-nav me-auto mb-2 mb-lg-0">
						<li className="nav-item">
							<NavLink to="/" title="Accueil" className="nav-link">
								Accueil
							</NavLink>
						</li>
						{user && (
							<li className="nav-item">
								<NavLink to="/dashboard" title="Tableau de bord" className="nav-link">
									Tableau de bord
								</NavLink>
							</li>
						)}
					</ul>

					<ul className="navbar-nav ml-auto">
						{!user ? (
							<>
								<li className="nav-item">
									<a
										className="nav-link"
										onClick={() => {
											setLoginModal(true);
											setIsLogin(true);
										}}>
										Connexion
									</a>

									<Modal
										title="Connexion"
										buttonName="Se connecter"
										isOpen={loginModal}
										toggle={() => setLoginModal(!loginModal)}
										handleClose={() => setLoginModal(false)}
										handleValid={handleSubmit}>
										<div className="input-group mb-4">
											<span className="input-group-text" id="basic-addon1">
												@
											</span>
											<input onChange={handleChange} name="email" type="text" className="form-control" placeholder="Email" />
										</div>

										<div className="input-group mb-4">
											<input
												onChange={handleChange}
												name="password"
												type="password"
												className="form-control"
												placeholder="Mot de passe"
											/>
										</div>
									</Modal>
								</li>
								<li className="nav-item">
									<a
										className="nav-link"
										onClick={() => {
											setRegistrationModal(true);
											setIsLogin(false);
										}}>
										Inscription
									</a>

									<Modal
										title="Inscription"
										isOpen={registrationModal}
										buttonName="Créer mon compte"
										toggle={() => setRegistrationModal(!registrationModal)}
										handleClose={() => setRegistrationModal(false)}
										handleValid={handleSubmit}>
										<div className="input-group mt-4 mb-4">
											<input
												onChange={handleChange}
												type="text"
												name="username"
												value={values.username}
												aria-label="Nom"
												placeholder="Nom"
												className="form-control"
											/>
										</div>

										<div className="input-group mb-4">
											<span className="input-group-text" id="basic-addon1">
												@
											</span>
											<input
												onChange={handleChange}
												name="email"
												type="text"
												value={values.email}
												className="form-control"
												placeholder="Email"
												aria-label="Email address"
												aria-describedby="basic-addon1"
											/>
										</div>

										<div className="input-group mb-4">
											<input
												onChange={handleChange}
												value={values.password}
												name="password"
												type="password"
												className="form-control"
												placeholder="Mot de passe"
											/>
										</div>
									</Modal>
								</li>

								<AddAdmin />
							</>
						) : (
							<>
								<li className="nav-item dropdown">
									<a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">
										Panier{" "}
										<span className={`badge rounded-pill bg-${cart.length > 0 ? "danger" : "secondary"}`}>{cart.length}</span>
									</a>
									<ul className="dropdown-menu">
										{cart.length > 0 ? (
											cart.map((c, i) => (
												<li key={c.name}>
													<a className="dropdown-item disabled">
														{c.name} - {c.price}€
													</a>
													<a
														onClick={() =>
															dispatch(
																removeFromCart({
																	pizza: c,
																})
															)
														}
														className="btn dropdown-item">
														<b>Retirer du panier</b>
													</a>
													{cart.length - 1 !== i && <hr />}
												</li>
											))
										) : (
											<li>
												<a className="dropdown-item disabled">Votre panier est vide.</a>
											</li>
										)}
										{cart.length > 0 && (
											<>
												<li>
													<hr className="dropdown-divider" />
												</li>
												<li>
													<a onClick={handleOrder} className="btn dropdown-item">
														<b>Validez le panier</b>
													</a>
													<a onClick={() => dispatch(clearCart())} className="dropdown-item disabled">
														<b>Total: {total}€</b>
													</a>
												</li>
											</>
										)}
									</ul>
								</li>
								<li className="nav-item">
									<a className="nav-link" onClick={logout}>
										Deconnexion
									</a>
								</li>
							</>
						)}
					</ul>
				</div>
			</div>
		</nav>
	);
}

export default Navbar;
