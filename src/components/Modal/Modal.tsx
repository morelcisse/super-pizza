import { ReactNode } from "react";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";

interface ModalProps {
	children: ReactNode;
	title: string;
	buttonName: string;
	isOpen: boolean;
	toggle: () => void;
	handleValid: () => void;
	handleClose: () => void;
}

function ModalComponent({ isOpen, children, title, buttonName, toggle, handleValid, handleClose }: ModalProps) {
	return (
		<Modal isOpen={isOpen} toggle={toggle}>
			<ModalHeader toggle={toggle}>{title}</ModalHeader>
			<ModalBody>{children}</ModalBody>
			<ModalFooter>
				<Button color="secondary" onClick={handleClose}>
					Fermer
				</Button>
				<Button color="primary" onClick={handleValid}>
					{buttonName}
				</Button>
			</ModalFooter>
		</Modal>
	);
}

export default ModalComponent;
