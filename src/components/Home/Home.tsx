import { useEffect } from "react";
import Swal from "sweetalert2";

import Loading from "src/components/Loading/Loading";
import { truncate } from "src/helpers/Helpers";
import { IPizza } from "src/interfaces";

import { useDispatch, useSelector } from "src/redux";
import { addToCart } from "src/redux/cartReducer";
import axiosInstance from "src/helpers/Axios";
import { setPizzas, setPizzasLoading } from "src/redux/pizzasReducer";

import "./Home.scss";

function Home() {
	const token = useSelector(({ auth }) => auth.token);
	const loading = useSelector(({ pizzas }) => pizzas.loadingPizzas);
	const pizzas = useSelector(({ pizzas }) => pizzas.pizzas);
	const dispatch = useDispatch();

	const handleClick = (pizza: IPizza) => {
		if (!token) {
			return Swal.fire({
				title: "Info",
				text: "Veuillez créer un compte ou vous connectez pour pouvoir passer commande.",
				icon: "info",
				confirmButtonText: "OK",
			});
		}

		return dispatch(addToCart({ pizza }));
	};

	const fetch = async () => {
		dispatch(setPizzasLoading(true));
		await axiosInstance()
			.get("/pizzas")
			.then(({ data }) => {
				const { pizzas } = data;
				dispatch(setPizzas({ pizzas }));
			});
	};

	useEffect(() => {
		fetch();
	}, []);

	if (loading) return <Loading />;

	return (
		<div id="home" className="container">
			<h3>Liste des PIZZAs</h3>

			<div className="row" id="pizzas-container">
				{pizzas.length <= 0 ? (
					<div className="alert alert-primary" role="alert">
						Aucune pizza à afficher
					</div>
				) : (
					pizzas.map((pizza: IPizza) => (
						<div key={pizza._id} className="col-4">
							<div className="card" style={{ width: "18rem" }}>
								<img src={pizza.img_path} className="card-img-top" alt={pizza.name} />
								<div className="card-body">
									<h5 className="card-title">Pizza: {pizza.name}</h5>
									<p className="card-text">
										<span data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title={pizza.description}>
											{truncate(pizza.description, 10, "...")}
										</span>
									</p>

									<hr />

									<table>
										<tbody>
											<tr>
												<td>
													<b>Qty:</b>
												</td>
												<td> {pizza.qty} </td>
											</tr>
										</tbody>
									</table>

									<a onClick={() => handleClick(pizza)} title="Panier" className="btn btn-success">
										Ajouter au panier
									</a>
								</div>
							</div>
						</div>
					))
				)}
			</div>
		</div>
	);
}

export default Home;
